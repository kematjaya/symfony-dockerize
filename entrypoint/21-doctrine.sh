#!/bin/bash
/usr/local/bin/php /app/bin/console doctrine:schema:update --force
/usr/local/bin/php /app/bin/console doctrine:fixtures:load --no-interaction
