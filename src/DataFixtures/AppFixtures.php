<?php

namespace App\DataFixtures;

use App\Entity\Student;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $data = [
            ["name" => "Ahmaderta R", "address" => "Madiun"],
            ["name" => "Zulham Effendi", "address" => "Sidoarjo"],
            ["name" => "Nur H", "address" => "Lamongan"],
            ["name" => "Agung A", "address" => "Sidoarjo"],
        ];
       foreach ($data as $row) {
           $entity = new Student();
           $entity->setName($row["name"]);
           $entity->setAddress($row["address"]);
           $manager->persist($entity);
       }

        $manager->flush();
    }
}
