<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controller;

use App\Entity\Student;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Description of HomepageController
 *
 * @author programmer
 */
class HomepageController extends AbstractController 
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(ManagerRegistry $managerRegistry): Response
    {
        $entities = $managerRegistry->getRepository(Student::class)->findAll();
        $students = [];
        foreach ($entities as $entity) {
            $students[] = [
                "name" => $entity->getName(),
                "address" => $entity->getAddress()
            ];
        }
        return $this->json($students);
    }
}
