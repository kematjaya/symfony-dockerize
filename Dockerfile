FROM webdevops/php-nginx:7.4

ENV WEB_DOCUMENT_ROOT=/app/public
RUN apt-get update -y && apt-get install -y curl unzip

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /app
COPY bin bin
COPY config config
COPY migrations migrations
COPY public public
COPY src src
COPY composer.json composer.json
COPY .env .env

COPY entrypoint /opt/docker/provision/entrypoint.d
RUN chmod +x /opt/docker/provision/entrypoint.d/21-doctrine.sh

RUN composer update


#CMD ["/app/entrypoint/doctrine.sh"]
